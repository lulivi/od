use csv::{Reader, ReaderBuilder, Trim};
use serde::Deserialize;
use std::collections::HashMap;

/// Fixed value information
#[derive(Deserialize)]
pub struct PatternFixedValues {
    pub name: String,
    pub d17_16: f64,
    pub d18_16: f64,
    #[serde(rename = "dD_H")]
    pub dd_h: f64,
}

/// Read the contents of the fixed values CSV and load it into a dictionary.
///
/// # Example
///
/// ```
/// use csv::{Reader, ReaderBuilder, Trim};
/// use std::collections::HashMap;
/// use std::error::Error;
/// use od::utils::read_fixed_values;
///
/// # fn main() { example().unwrap(); }
/// fn example() -> Result<(), Box<dyn Error>> {
///     let data = "\
///         name, d17_16, d18_16,  dD_H   \n\
///         fv1,   -3.11,  37.82, 3310.30 \n\
///         fv2,   83.09, -91.05,  328.00 \n\
///     ";
///     let obtained: HashMap<String, HashMap<String, f64>> = read_fixed_values(data)?;
///
///     assert_eq!(obtained.get("d18_16").unwrap().get("fv1").unwrap(), &37.82);
///     assert_eq!(obtained.get("d18_16").unwrap().get("fv2").unwrap(), &-91.05);
///     assert_eq!(obtained.get("dD_H").unwrap().get("fv1").unwrap(), &3310.30);
///     assert_eq!(obtained.get("dD_H").unwrap().get("fv2").unwrap(), &328.00);
///
///     Ok(())
/// }
/// ```
pub fn read_fixed_values(
    file_content: &str,
) -> Result<HashMap<String, HashMap<String, f64>>, String> {
    let mut fixed_values: HashMap<String, HashMap<String, f64>> = HashMap::with_capacity(3);
    let mut fixed_values_d17_16: HashMap<String, f64> = HashMap::with_capacity(16);
    let mut fixed_values_d18_16: HashMap<String, f64> = HashMap::with_capacity(16);
    let mut fixed_values_dh_h: HashMap<String, f64> = HashMap::with_capacity(16);
    let mut rdr: Reader<&[u8]> = ReaderBuilder::new()
        .trim(Trim::All)
        .comment(Some(b'#'))
        .from_reader(file_content.as_bytes());
    let result_iter = rdr.deserialize();

    for result in result_iter {
        let record: PatternFixedValues = match result {
            Ok(record) => record,
            Err(err) => return Err(format!("{}", err)),
        };
        fixed_values_d17_16.insert(record.name.clone(), record.d17_16);
        fixed_values_d18_16.insert(record.name.clone(), record.d18_16);
        fixed_values_dh_h.insert(record.name.clone(), record.dd_h);
    }

    fixed_values.insert("d17_16".to_string(), fixed_values_d17_16);
    fixed_values.insert("d18_16".to_string(), fixed_values_d18_16);
    fixed_values.insert("dD_H".to_string(), fixed_values_dh_h);

    Ok(fixed_values)
}
