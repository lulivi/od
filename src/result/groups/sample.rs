use super::Group;
use crate::result::isotopes::SampleIsotope;
use crate::result::schema::CleanRecord;
use crate::result::utils::{PowerVector, PowerVectorTrait};
use crate::stat::LinEqResult;

use delegate::delegate;

pub struct SampleGroup {
    common: Group<SampleIsotope>,
}

impl SampleGroup {
    pub fn new(queue: &[CleanRecord]) -> Self {
        Self {
            common: Group::<SampleIsotope>::new(queue),
        }
    }
    delegate! {
        to self.common {
            pub fn append(&mut self, queue: &[CleanRecord]);
            pub fn inj_nr(&self) -> &Vec<u8>;
            pub fn d17_16(&self) -> &SampleIsotope;
            pub fn d17_16_mut(&mut self) -> &mut SampleIsotope;
            pub fn d18_16(&self) -> &SampleIsotope;
            pub fn d18_16_mut(&mut self) -> &mut SampleIsotope;
            pub fn dd_h(&self) -> &SampleIsotope;
            pub fn dd_h_mut(&mut self) -> &mut SampleIsotope;
            pub fn identifier_1(&self) -> &String;
            pub fn len(&self) -> usize;
        }
    }
    pub fn perform_calcs(&mut self) {
        self.d17_16_mut().perform_calcs();
        self.d18_16_mut().perform_calcs();
        self.dd_h_mut().perform_calcs();
    }
    pub fn perform_corrections(
        &mut self,
        d18_16_lin_eq_result: &LinEqResult,
        dd_h_lin_eq_result: &LinEqResult,
        d17_16_lin_eq_vsmow_slap: &Option<LinEqResult>,
        d18_16_lin_eq_vsmow_slap: &Option<LinEqResult>,
    ) {
        if d17_16_lin_eq_vsmow_slap.is_some() && d18_16_lin_eq_vsmow_slap.is_some() {
            self.d17_16_mut()
                .perform_calibration(&d17_16_lin_eq_vsmow_slap.unwrap());
            self.d18_16_mut()
                .perform_calibration(&d18_16_lin_eq_vsmow_slap.unwrap());
            {
                let mut d17_16_excess = PowerVector::with_capacity(self.len());
                for i in 0..self.len() {
                    let d17_16_ln =
                        (self.d17_16().calibrated().as_ref().unwrap()[i] / 1000.0f64).ln_1p();
                    let d18_16_ln =
                        (self.d18_16().calibrated().as_ref().unwrap()[i] / 1000.0f64).ln_1p();
                    d17_16_excess.push((d17_16_ln - 0.528f64 * d18_16_ln) * 1000000f64);
                }
                d17_16_excess.perform_calcs();
                self.d17_16_mut().set_excess(Some(d17_16_excess));
            }
        }

        self.d18_16_mut().perform_corrections(d18_16_lin_eq_result);

        self.dd_h_mut().perform_corrections(dd_h_lin_eq_result);
        let mut dd_h_excess = PowerVector::with_capacity(self.len());
        for i in 0..self.len() {
            dd_h_excess.push(
                self.dd_h().corrected().as_ref().unwrap()[i]
                    - self.d18_16().corrected().as_ref().unwrap()[i] * 8.0f64,
            );
        }
        dd_h_excess.perform_calcs();
        self.dd_h_mut().set_excess(Some(dd_h_excess));
    }
}
