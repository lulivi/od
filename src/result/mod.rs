mod groups;
mod isotopes;
pub mod schema;
mod utils;

use crate::stat::{linear_equation, r_squared, LinEqMatrix, LinEqResult};
use groups::{PatternGroup, SampleGroup};
use schema::{CleanRecord, InputRecord};

use csv::{DeserializeRecordsIter, Reader, ReaderBuilder, Trim, Writer, WriterBuilder};
use std::collections::{HashMap, VecDeque};
use std::error::Error;
use std::fs::File;

pub struct CalculationResults {
    input_name: String,
    n_patterns: usize,
    n_samples: usize,
    patterns: HashMap<String, PatternGroup>,
    patterns_order: Vec<String>,
    samples: HashMap<String, SampleGroup>,
    samples_order: Vec<String>,
    fixed_values: HashMap<String, HashMap<String, f64>>,
    d18_16_lin_eq_result: LinEqResult,
    dd_h_lin_eq_result: LinEqResult,
    d17_16_lin_eq_vsmow_slap_result: Option<LinEqResult>,
    d18_16_lin_eq_vsmow_slap_result: Option<LinEqResult>,
    d18_16_lin_eq_r_square: f64,
    dd_h_lin_eq_r_square: f64,
    selected_patterns: [String; 2],
}

impl CalculationResults {
    /// Creates a new instance of `CalculationResults`
    ///
    /// # Arguments:
    ///
    /// * `input_name` - name of the file containing all the data
    /// * `fixed_values` - dictionary of fixed values
    ///
    /// # Examles:
    ///
    /// ```no_run
    /// use std::error::Error;
    /// use od::result::result::CalculationResults;
    /// use std::collections::HashMap;
    /// use od::utils::read_fixed_values;
    ///
    /// # fn main() { example().unwrap(); }
    /// fn example() -> Result<(), Box<dyn Error>> {
    ///     let data = "\
    ///     name,   d18_16,    dD_H\n \
    ///     fv1,   37.82,   3310.30\n \
    ///     fv2,  -91.05,    328.00 \
    ///     ";
    ///     let fv: HashMap<String, HashMap<String, f64>> = read_fixed_values(data)?;
    ///     let mut calculation_results = CalculationResults::new("foo.csv", fv);
    ///
    ///     calculation_results.load_csv()?;
    ///     calculation_results.perform_calcs()?;
    ///     calculation_results.serialize()?;
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn new(
        input_name: &str,
        n_patterns: usize,
        n_samples: usize,
        fixed_values: HashMap<String, HashMap<String, f64>>,
    ) -> CalculationResults {
        CalculationResults {
            input_name: input_name.to_string(),
            n_patterns,
            n_samples,
            patterns: HashMap::with_capacity(3),
            patterns_order: Vec::<String>::with_capacity(3),
            samples: HashMap::with_capacity(3),
            samples_order: Vec::<String>::with_capacity(3),
            fixed_values,
            d18_16_lin_eq_result: LinEqResult {
                slope: 0f64,
                intercept: 0f64,
            },
            dd_h_lin_eq_result: LinEqResult {
                slope: 0f64,
                intercept: 0f64,
            },
            d17_16_lin_eq_vsmow_slap_result: None,
            d18_16_lin_eq_vsmow_slap_result: None,
            d18_16_lin_eq_r_square: 0f64,
            dd_h_lin_eq_r_square: 0f64,
            selected_patterns: ["VSMOW2".to_string(), "SLAP2".to_string()],
        }
    }
    /// Pushes a new record into the queue
    ///
    /// # Arguments:
    ///
    /// * `record` - the complete record holding all the values
    /// * `queue` - the queue holding the last n records
    fn save_to_queue(
        record: &InputRecord,
        queue: &mut VecDeque<CleanRecord>,
    ) -> Result<(), Box<dyn Error>> {
        let clean_record = CleanRecord {
            inj_nr: record.inj_nr,
            d17_16: record.d17_16,
            d18_16: record.d18_16,
            dd_h: record.dd_h,
            identifier_1: record.identifier_1.clone(),
        };
        queue.push_back(clean_record);
        if queue.len() > 7 {
            queue.pop_front();
        }
        Ok(())
    }
    /// Parses a complete CSV to obtain a reduced one
    ///
    /// # Arguments:
    ///
    /// * `input_name` - holds the name of the input CSV
    pub fn load_csv(&mut self) -> Result<(), Box<dyn Error>> {
        let input_file = match File::open(&self.input_name) {
            Ok(input_file) => input_file,
            Err(err) => {
                return Err(From::from(format!(
                    "No se pudo abrir el archivo de entrada '{}' ({})",
                    &self.input_name, err
                )))
            }
        };
        let output_name = &self.input_name.replace(".csv", "_clean.csv");
        let output_file = match File::create(&output_name) {
            Ok(output_file) => output_file,
            Err(err) => {
                return Err(From::from(format!(
                    "No se pudo crear el archivo '{}' ({})",
                    &output_name, err
                )))
            }
        };

        println!(
            "=> Cargando datos del archivo `{}` y guardandolos en `{}`...",
            &self.input_name, &output_name
        );

        let mut rdr: Reader<File> = ReaderBuilder::new().trim(Trim::All).from_reader(input_file);
        let mut wtr: Writer<File> = WriterBuilder::new().from_writer(output_file);
        let mut queue: VecDeque<CleanRecord> = VecDeque::with_capacity(3);
        let mut result_iter: DeserializeRecordsIter<'_, File, InputRecord> = rdr.deserialize();

        // Read the first record to get rid of the case in where the queue is empty
        let mut current_identifier: String = match result_iter.next() {
            Some(result) => {
                let record: InputRecord = match result {
                    Ok(record) => record,
                    Err(error) => {
                        return Err(From::from(format!("Error leyendo la primera linea: {:?}", error)))
                    }
                };
                wtr.serialize(&record)?;
                CalculationResults::save_to_queue(&record, &mut queue)?;
                record.identifier_1.clone()
            }
            None => return Err(From::from("Ocurrió error leyendo el archivo csv")),
        };

        // Loop over every line in the csv and serialize the chosen columns
        for result in result_iter {
            // Ignore the record if it is empty or has an error on it
            let record: InputRecord = match result {
                Ok(record) => record,
                Err(error) => {
                    if let csv::ErrorKind::Deserialize { pos, err } = error.into_kind() {
                        let position: csv::Position = pos.unwrap();
                        println!(
                            "Error leyendo el campo {} del registro {} en la línea {}. Ignorándolo...",
                            err.field().unwrap(),
                            position.record(),
                            position.line(),
                        )
                    };
                    continue;
                }
            };
            wtr.serialize(&record)?;

            // Save the value in the queue if there is no identifier change
            if current_identifier == record.identifier_1 {
                CalculationResults::save_to_queue(&record, &mut queue)?;
                continue;
            }

            // Save the current group and update the current_identifier
            self.add_group(&mut queue)?;
            queue.clear();
            CalculationResults::save_to_queue(&record, &mut queue)?;
            current_identifier = record.identifier_1.clone();
        }
        self.add_group(&mut queue)?;
        println!(
            "=> Cargando datos del archivo `{}` y guardándolos en `{}`... Hecho.",
            &self.input_name, &output_name
        );

        Ok(())
    }
    fn add_group(&mut self, queue: &mut VecDeque<CleanRecord>) -> Result<(), String> {
        queue.make_contiguous();
        let group_identifier = &queue.front().unwrap().identifier_1;

        if self.patterns.keys().len() < 3 || self.patterns.contains_key(group_identifier) {
            return self
                .add_pattern_group(group_identifier, &queue.as_slices().0[0..self.n_patterns]);
        }
        self.add_sample_group(group_identifier, &queue.as_slices().0[0..self.n_samples])
    }
    fn add_pattern_group(
        &mut self,
        group_identifier: &str,
        queue: &[CleanRecord],
    ) -> Result<(), String> {
        match self.patterns.get_mut(group_identifier) {
            Some(group) => group.append(queue),
            None => {
                for record in queue.iter() {
                    if !self
                        .fixed_values
                        .get("d18_16")
                        .unwrap()
                        .contains_key(&record.identifier_1)
                    {
                        return Err(format!(
                            "'{}' no es un identificador de patrón válido. Los posibles valores \
                            son: {:?}.",
                            record.identifier_1,
                            &self.fixed_values.get("d18_16").unwrap().keys()
                        ));
                    }
                }
                self.patterns
                    .insert(group_identifier.to_string(), PatternGroup::new(queue));
                self.patterns_order.push(group_identifier.to_string());
            }
        }
        Ok(())
    }
    fn add_sample_group(
        &mut self,
        group_identifier: &str,
        queue: &[CleanRecord],
    ) -> Result<(), String> {
        match self.samples.get_mut(group_identifier) {
            Some(group) => group.append(queue),
            None => {
                self.samples
                    .insert(group_identifier.to_string(), SampleGroup::new(queue));
                self.samples_order.push(group_identifier.to_string())
            }
        }
        Ok(())
    }
    pub fn serialize(&self) -> Result<(), Box<dyn Error>> {
        println!("=> Guardando los resultados...");
        self.serialize_patterns()?;
        self.serialize_samples()?;
        println!("=> Guardando los resultados... Hecho.");

        Ok(())
    }
    fn get_csv_output_name(&self, suffix: &str) -> Result<String, Box<dyn Error>> {
        Ok(format!(
            "{}{}.csv",
            &self.input_name.trim_end_matches(".csv"),
            suffix
        ))
    }
    fn get_csv_writter(&self, output_name: &str) -> Result<Writer<File>, Box<dyn Error>> {
        let output_file = match File::create(output_name) {
            Ok(output_file) => output_file,
            Err(err) => {
                return Err(From::from(format!(
                    "No se pudo crear el archivo '{}' ({})",
                    output_name, err
                )))
            }
        };
        Ok(WriterBuilder::new()
            .has_headers(true)
            .from_writer(output_file))
    }
    fn get_fixed_value(&self, isotope: &str, group_identifier: &str) -> Result<f64, String> {
        match self.fixed_values.get(isotope) {
            Some(isotope_fv) => match isotope_fv.get(group_identifier) {
                Some(fixed_val) => Ok(*fixed_val),
                None => {
                    return Err(format!(
                        "El patrón '{}' no pudo ser encontrado en la tabla de valores fijos del \
                            isótopo 'd{}'.\nLos valores posibles son: {:?}",
                        group_identifier,
                        isotope,
                        &self.fixed_values.get(isotope).unwrap().keys()
                    ))
                }
            },
            None => Err(format!(
                "El isótopo 'd{}' no fue encontrado en el conjunto de valores fijos. Posibles \
                valores: {:?}",
                isotope,
                self.fixed_values.keys()
            )),
        }
    }
    fn write_record(
        &self,
        wtr: &mut Writer<File>,
        base_list: Vec<&str>,
    ) -> Result<(), Box<dyn Error>> {
        wtr.write_record(
            [
                base_list.as_slice(),
                &[""].repeat(20usize - base_list.len()),
            ]
            .concat(),
        )?;
        Ok(())
    }
    fn serialize_patterns_3_lineal_equation(
        &self,
        wtr: &mut Writer<File>,
    ) -> Result<(), Box<dyn Error>> {
        self.write_record(wtr, vec![])?;
        self.write_record(
            wtr,
            vec![
                "Ajuste a tres",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
            ],
        )?;
        self.write_record(
            wtr,
            vec![
                "Pendiente",
                "",
                &self.d18_16_lin_eq_result.slope.to_string(),
                "",
                "",
                &self.dd_h_lin_eq_result.slope.to_string(),
            ],
        )?;
        self.write_record(
            wtr,
            vec![
                "Ordenada en el orgigen",
                "",
                &self.d18_16_lin_eq_result.intercept.to_string(),
                "",
                "",
                &self.dd_h_lin_eq_result.intercept.to_string(),
            ],
        )?;
        self.write_record(
            wtr,
            vec![
                "R cuadrado",
                "",
                &self.d18_16_lin_eq_r_square.to_string(),
                "",
                "",
                &self.dd_h_lin_eq_r_square.to_string(),
            ],
        )?;
        self.write_record(wtr, vec![])?;

        self.write_record(wtr, vec!["d(18_16)Mean", "x", "y"])?;
        for group_identifier in self.patterns_order.iter() {
            self.write_record(
                wtr,
                vec![
                    group_identifier,
                    &self
                        .patterns
                        .get(group_identifier)
                        .unwrap()
                        .d18_16()
                        .mean()
                        .unwrap()
                        .to_string(),
                    &self
                        .get_fixed_value("d18_16", group_identifier)?
                        .to_string(),
                ],
            )?;
        }
        self.write_record(wtr, vec![])?;
        self.write_record(wtr, vec!["d(D_H)Mean"])?;
        for group_identifier in self.patterns_order.iter() {
            self.write_record(
                wtr,
                vec![
                    group_identifier,
                    &self
                        .patterns
                        .get(group_identifier)
                        .unwrap()
                        .dd_h()
                        .mean()
                        .unwrap()
                        .to_string(),
                    &self.get_fixed_value("dD_H", group_identifier)?.to_string(),
                ],
            )?;
        }
        Ok(())
    }
    fn serialize_patterns_2_lineal_equation(
        &self,
        wtr: &mut Writer<File>,
    ) -> Result<(), Box<dyn Error>> {
        self.write_record(wtr, vec![])?;
        self.write_record(
            wtr,
            vec![
                "Ajuste a dos",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
                "-----",
            ],
        )?;
        self.write_record(wtr, vec![])?;
        self.write_record(
            wtr,
            vec![
                "Pendiente",
                &self
                    .d17_16_lin_eq_vsmow_slap_result
                    .unwrap()
                    .slope
                    .to_string(),
                &self
                    .d18_16_lin_eq_vsmow_slap_result
                    .unwrap()
                    .slope
                    .to_string(),
            ],
        )?;
        self.write_record(
            wtr,
            vec![
                "Ordenada en el orgigen",
                &self
                    .d17_16_lin_eq_vsmow_slap_result
                    .unwrap()
                    .intercept
                    .to_string(),
                &self
                    .d18_16_lin_eq_vsmow_slap_result
                    .unwrap()
                    .intercept
                    .to_string(),
            ],
        )?;
        self.write_record(wtr, vec![])?;
        self.write_record(wtr, vec!["d(17_16)Mean", "x", "y"])?;
        for group_identifier in self
            .patterns_order
            .iter()
            .filter(|k| self.selected_patterns.contains(k))
        {
            self.write_record(
                wtr,
                vec![
                    group_identifier,
                    &self
                        .patterns
                        .get(group_identifier)
                        .unwrap()
                        .d17_16()
                        .mean()
                        .unwrap()
                        .to_string(),
                    &self
                        .get_fixed_value("d17_16", group_identifier)?
                        .to_string(),
                ],
            )?;
        }
        self.write_record(wtr, vec![])?;
        self.write_record(wtr, vec!["d(18_16)Mean", "x", "y"])?;
        for group_identifier in self
            .patterns_order
            .iter()
            .filter(|k| self.selected_patterns.contains(k))
        {
            self.write_record(
                wtr,
                vec![
                    group_identifier,
                    &self
                        .patterns
                        .get(group_identifier)
                        .unwrap()
                        .d18_16()
                        .mean()
                        .unwrap()
                        .to_string(),
                    &self
                        .get_fixed_value("d18_16", group_identifier)?
                        .to_string(),
                ],
            )?;
        }
        Ok(())
    }
    fn serialize_patterns(&self) -> Result<(), Box<dyn Error>> {
        let output_name = self.get_csv_output_name("_patterns")?;
        println!("=====> Guardando '{}'...", &output_name);
        let mut wtr = self.get_csv_writter(&output_name)?;
        for group_identifier in self.patterns_order.iter() {
            match self.patterns.get(group_identifier) {
                Some(group) => {
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Inj Nr",
                            "d(17_16)",
                            "d(18_16)",
                            "d(18_16) corr",
                            "d(18_16) mean - corr",
                            "d(D_H)",
                            "d(D_H) corr",
                            "d(D_H) mean - corr",
                            "Identifier 1",
                        ],
                    )?;
                    for i in 0..group.inj_nr().len() {
                        self.write_record(
                            &mut wtr,
                            vec![
                                &group.inj_nr()[i].to_string(),
                                &group.d17_16().isotope()[i].to_string(),
                                &group.d18_16().isotope()[i].to_string(),
                                &match group.d18_16().corrected() {
                                    Some(pvector) => pvector[i].to_string(),
                                    None => "".to_string(),
                                },
                                &match group.d18_16().difference() {
                                    Some(pvector) => pvector[i].to_string(),
                                    None => "".to_string(),
                                },
                                &group.dd_h().isotope()[i].to_string(),
                                &match group.dd_h().corrected() {
                                    Some(pvector) => pvector[i].to_string(),
                                    None => "".to_string(),
                                },
                                &match group.dd_h().difference() {
                                    Some(pvector) => pvector[i].to_string(),
                                    None => "".to_string(),
                                },
                                group.identifier_1(),
                            ],
                        )?;
                    }
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Media",
                            &group.d17_16().mean().unwrap().to_string(),
                            &group.d18_16().mean().unwrap().to_string(),
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.mean().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().difference() {
                                Some(pvector) => pvector.mean().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &group.dd_h().mean().unwrap().to_string(),
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.mean().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().difference() {
                                Some(pvector) => pvector.mean().unwrap().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Desviación típica",
                            &group.d17_16().std().unwrap().to_string(),
                            &group.d18_16().std().unwrap().to_string(),
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.std().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().difference() {
                                Some(pvector) => pvector.std().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &group.dd_h().std().unwrap().to_string(),
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.std().unwrap().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().difference() {
                                Some(pvector) => pvector.std().unwrap().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                }
                None => {
                    println!(
                        "WARNING: El identificador de patrón '{}' no se encontró en la lista de \
                            patrones guardados. Omitiéndolo...",
                        group_identifier
                    );
                    continue;
                }
            }
            self.write_record(
                &mut wtr,
                vec![
                    "Valor fijo",
                    "",
                    &self
                        .get_fixed_value("d18_16", group_identifier)?
                        .to_string(),
                    "",
                    "",
                    &self.get_fixed_value("dD_H", group_identifier)?.to_string(),
                ],
            )?;
            self.write_record(&mut wtr, vec![])?;
        }
        self.serialize_patterns_3_lineal_equation(&mut wtr)?;
        if self
            .selected_patterns
            .iter()
            .all(|k| self.patterns.contains_key(k))
        {
            self.serialize_patterns_2_lineal_equation(&mut wtr)?;
        }
        self.write_record(&mut wtr, vec![])?;
        self.write_record(
            &mut wtr,
            vec![
                "Identifier 1",
                "d(17_16) mean",
                "d(17_16) std",
                "d(18_16) mean",
                "d(18_16) std",
                "d(18_16) corr mean",
                "d(18_16) corr std",
                "d(D_H) mean",
                "d(D_H) std",
                "d(D_H) corr mean",
                "d(D_H) corr std",
            ],
        )?;
        for group_identifier in self.patterns_order.iter() {
            match self.patterns.get(group_identifier) {
                Some(group) => {
                    self.write_record(
                        &mut wtr,
                        vec![
                            group_identifier,
                            &group.d17_16().mean().unwrap().to_string(),
                            &group.d17_16().std().unwrap().to_string(),
                            &group.d18_16().mean().unwrap().to_string(),
                            &group.d18_16().std().unwrap().to_string(),
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.dd_h().mean().unwrap().to_string(),
                            &group.dd_h().std().unwrap().to_string(),
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                }
                None => {
                    println!(
                        "WARNING: El identificador de patrón '{}' no se encontró en la lista de \
                            patrones guardados. Omitiéndolo...",
                        group_identifier
                    );
                    continue;
                }
            }
        }
        println!("=====> Guardando '{}'... Hecho.", &output_name);
        Ok(())
    }
    fn serialize_samples(&self) -> Result<(), Box<dyn Error>> {
        let output_name = self.get_csv_output_name("_samples")?;
        println!("=====> Guardando '{}'...", &output_name);
        let mut wtr = self.get_csv_writter(&output_name)?;
        for group_identifier in self.samples_order.iter() {
            match self.samples.get(group_identifier) {
                Some(group) => {
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Inj Nr",
                            "d(17_16)",
                            "d(17_16) calib",
                            "d(17_16) excess",
                            "d(18_16)",
                            "d(18_16) corr",
                            "d(18_16) calib",
                            "d(D_H)",
                            "d(D_H) corr",
                            "d(D_H) excess",
                            "Identifier 1",
                        ],
                    )?;
                    for i in 0..group.inj_nr().len() {
                        self.write_record(
                            &mut wtr,
                            vec![
                                &group.inj_nr()[i].to_string(),
                                &group.d17_16().isotope()[i].to_string(),
                                &match group.d17_16().calibrated() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &match group.d17_16().excess() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &group.d18_16().isotope()[i].to_string(),
                                &match group.d18_16().corrected() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &match group.d18_16().calibrated() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &group.dd_h().isotope()[i].clone().to_string(),
                                &match group.dd_h().corrected() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &match group.dd_h().excess() {
                                    Some(pvector) => pvector[i].clone().to_string(),
                                    None => "".to_string(),
                                },
                                &group.identifier_1().clone(),
                            ],
                        )?
                    }
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Media",
                            &group.d17_16().isotope().mean().unwrap().to_string(),
                            &match group.d17_16().calibrated() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d17_16().excess() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.d18_16().isotope().mean().unwrap().to_string(),
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().calibrated() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.dd_h().isotope().mean().unwrap().clone().to_string(),
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().excess() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                    self.write_record(
                        &mut wtr,
                        vec![
                            "Desviación típica",
                            &group.d17_16().isotope().std().unwrap().to_string(),
                            &match group.d17_16().calibrated() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d17_16().excess() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.d18_16().isotope().std().unwrap().to_string(),
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().calibrated() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.dd_h().isotope().std().unwrap().clone().to_string(),
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().excess() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                    self.write_record(&mut wtr, vec![])?;
                }
                None => {
                    println!(
                        "WARNING: El identificador de muestra '{}' no se encontró en la lista de \
                        patrones guardados. Omitiéndolo...",
                        group_identifier
                    );
                }
            }
        }
        self.write_record(
            &mut wtr,
            vec![
                "identifier_1",
                "d(18_16) corr mean",
                "d(18_16) corr std",
                "d(D_H) corr mean",
                "d(D_H) corr std",
                "d(D_H) excess mean",
                "d(17_16) mean",
                "d(17_16) std",
                "d(17_16) excess mean",
            ],
        )?;
        for group_identifier in self.samples_order.iter() {
            match self.samples.get(group_identifier) {
                Some(group) => {
                    self.write_record(
                        &mut wtr,
                        vec![
                            group_identifier,
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.d18_16().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().corrected() {
                                Some(pvector) => pvector.std().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &match group.dd_h().excess() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                            &group.d17_16().mean().unwrap().to_string(),
                            &group.d17_16().std().unwrap().to_string(),
                            &match group.d17_16().excess() {
                                Some(pvector) => pvector.mean().unwrap().clone().to_string(),
                                None => "".to_string(),
                            },
                        ],
                    )?;
                }
                None => {
                    println!(
                        "WARNING: El identificador de muestra '{}' no se encontró en la lista de \
                        patrones guardados. Omitiéndolo...",
                        group_identifier
                    );
                }
            }
        }
        println!("=====> Guardando '{}'... Hecho.", &output_name);
        Ok(())
    }
    pub fn perform_calcs(&mut self) -> Result<(), String> {
        println!("=> Calculando resultados... ");
        let mut d18_16_matrix = LinEqMatrix {
            x_values: Vec::<f64>::with_capacity(3),
            y_values: Vec::<f64>::with_capacity(3),
        };
        let mut dd_h_matrix = LinEqMatrix {
            x_values: Vec::<f64>::with_capacity(3),
            y_values: Vec::<f64>::with_capacity(3),
        };

        for group_identifier in self.patterns_order.iter() {
            let current_group_patterns = self.patterns.get_mut(group_identifier).unwrap();
            current_group_patterns.perform_calcs();
            d18_16_matrix
                .x_values
                .push(current_group_patterns.d18_16().mean().unwrap());
            dd_h_matrix
                .x_values
                .push(current_group_patterns.dd_h().mean().unwrap());
            d18_16_matrix
                .y_values
                .push(self.get_fixed_value("d18_16", group_identifier)?);
            dd_h_matrix
                .y_values
                .push(self.get_fixed_value("dD_H", group_identifier)?);
        }
        self.d18_16_lin_eq_result =
            linear_equation(&d18_16_matrix.x_values, &d18_16_matrix.y_values);
        self.dd_h_lin_eq_result = linear_equation(&dd_h_matrix.x_values, &dd_h_matrix.y_values);
        self.d18_16_lin_eq_r_square = r_squared(&d18_16_matrix.x_values, &d18_16_matrix.y_values);
        self.dd_h_lin_eq_r_square = r_squared(&dd_h_matrix.x_values, &dd_h_matrix.y_values);
        for group_identifier in self.patterns_order.iter() {
            let current_group_patterns = self.patterns.get_mut(group_identifier).unwrap();
            current_group_patterns
                .perform_corrections(&self.d18_16_lin_eq_result, &self.dd_h_lin_eq_result);
        }

        if self
            .selected_patterns
            .iter()
            .all(|k| self.patterns.contains_key(k))
        {
            let d17_16_vsmow_slap_matrix = LinEqMatrix {
                x_values: vec![
                    self.patterns
                        .get("VSMOW2")
                        .unwrap()
                        .d17_16()
                        .mean()
                        .unwrap(),
                    self.patterns.get("SLAP2").unwrap().d17_16().mean().unwrap(),
                ],
                y_values: vec![
                    self.get_fixed_value("d17_16", "VSMOW2")?,
                    self.get_fixed_value("d17_16", "SLAP2")?,
                ],
            };
            let d18_16_vsmow_slap_matrix = LinEqMatrix {
                x_values: vec![
                    self.patterns
                        .get("VSMOW2")
                        .unwrap()
                        .d18_16()
                        .mean()
                        .unwrap(),
                    self.patterns.get("SLAP2").unwrap().d18_16().mean().unwrap(),
                ],
                y_values: vec![
                    self.get_fixed_value("d18_16", "VSMOW2")?,
                    self.get_fixed_value("d18_16", "SLAP2")?,
                ],
            };
            self.d17_16_lin_eq_vsmow_slap_result = Some(linear_equation(
                &d17_16_vsmow_slap_matrix.x_values,
                &d17_16_vsmow_slap_matrix.y_values,
            ));
            self.d18_16_lin_eq_vsmow_slap_result = Some(linear_equation(
                &d18_16_vsmow_slap_matrix.x_values,
                &d18_16_vsmow_slap_matrix.y_values,
            ));
        }
        for group_identifier in self.samples.values_mut() {
            group_identifier.perform_calcs();
            group_identifier.perform_corrections(
                &self.d18_16_lin_eq_result,
                &self.dd_h_lin_eq_result,
                &self.d17_16_lin_eq_vsmow_slap_result,
                &self.d18_16_lin_eq_vsmow_slap_result,
            );
        }
        println!("=> Calculando resultados... Hecho.");
        Ok(())
    }
}
