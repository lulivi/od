pub struct LinEqMatrix {
    pub x_values: Vec<f64>,
    pub y_values: Vec<f64>,
}

#[derive(Copy, Clone)]
pub struct LinEqResult {
    pub slope: f64,
    pub intercept: f64,
}

/// Obtain the mean of a list of element
///
/// # Example
///
/// ```
/// use std::error::Error;
/// use od::stat::mean;
///
/// # fn main() { example().unwrap(); }
/// fn example() -> Result<(), Box<dyn Error>> {
///     let vector: Vec<f64> = vec![1.0, 2.0, 3.0, 4.0, 5.0];
///     let expected: f64 = 3.0;
///     let obtained: f64 = mean(&vector);
///
///     assert_eq!(expected, obtained);
///
///     Ok(())
/// }
/// ```
pub fn mean(values: &[f64]) -> f64 {
    if values.is_empty() {
        return 0f64;
    }

    return values.iter().sum::<f64>() / (values.len() as f64);
}

pub fn variance(values: &[f64]) -> f64 {
    if values.is_empty() {
        return 0f64;
    }

    let mean = mean(values);
    values
        .iter()
        .map(|x| f64::powf(x - mean, 2_f64))
        .sum::<f64>()
        / values.len() as f64
}

pub fn standard_deviation(values: &[f64]) -> f64 {
    if values.is_empty() {
        return 0f64;
    }

    let mean = mean(values);
    f64::powf(
        values
            .iter()
            .map(|x| f64::powf(x - mean, 2_f64))
            .sum::<f64>()
            / (values.len() as f64),
        0.5_f64,
    )
}

pub fn covariance(x_values: &[f64], y_values: &[f64]) -> f64 {
    if x_values.len() != y_values.len() {
        panic!("x_values and y_values must be of equal length.");
    }

    let length: usize = x_values.len();
    let mut covariance: f64 = 0f64;
    let mean_x = mean(x_values);
    let mean_y = mean(y_values);

    for i in 0..length {
        covariance += (x_values[i] - mean_x) * (y_values[i] - mean_y)
    }

    covariance / length as f64
}

pub fn r_squared(x_values: &[f64], y_values: &[f64]) -> f64 {
    if x_values.len() != y_values.len() {
        panic!("x_values and y_values must be of equal length.");
    }

    let sd_x = standard_deviation(x_values);
    let sd_y = standard_deviation(y_values);
    let cov = covariance(x_values, y_values);
    let r_value = cov / (sd_x * sd_y);

    r_value * r_value
}

pub fn linear_equation(x_values: &[f64], y_values: &[f64]) -> LinEqResult {
    let slope = covariance(x_values, y_values) / variance(x_values);
    let intercept = mean(y_values) - slope * mean(x_values);
    LinEqResult { slope, intercept }
}
