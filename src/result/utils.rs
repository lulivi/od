use crate::stat::{mean, variance};

use delegate::delegate;
use getset::Getters;
use std::ops::{Index, IndexMut};

pub trait PowerVectorTrait {
    fn with_capacity(size: usize) -> Self;
    fn push(&mut self, value: f64);
    fn iter(&self) -> std::slice::Iter<f64>;
    fn len(&self) -> usize;
}

#[derive(Getters)]
pub struct PowerVector {
    vec: Vec<f64>,
    #[getset(get = "pub")]
    mean: Option<f64>,
    #[getset(get = "pub")]
    std: Option<f64>,
}

impl Index<usize> for PowerVector {
    type Output = f64;

    delegate! {
        to self.vec {
            fn index(&self, idx: usize) -> &f64;
        }
    }
}

impl IndexMut<usize> for PowerVector {
    delegate! {
        to self.vec {
            fn index_mut(&mut self, idx: usize) -> &mut f64;
        }
    }
}

impl PowerVectorTrait for PowerVector {
    fn with_capacity(size: usize) -> PowerVector {
        PowerVector {
            vec: Vec::<f64>::with_capacity(size),
            mean: None,
            std: None,
        }
    }
    delegate! {
        to self.vec {
            fn push(&mut self, value: f64);
            fn iter(&self) -> std::slice::Iter<f64>;
            fn len(&self) -> usize;
        }
    }
}

impl PowerVector {
    pub fn perform_calcs(&mut self) {
        self.calc_mean();
        self.calc_std();
    }
    fn calc_mean(&mut self) {
        if self.len() == 0 {
            panic!("There is no data to calculate the mean!")
        }
        self.mean = Some(mean(&self.vec));
    }
    fn calc_std(&mut self) {
        if self.len() == 0 {
            panic!("There is no data to calculate the std!")
        }
        self.std = Some(variance(&self.vec).sqrt());
    }
}
