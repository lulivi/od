use crate::result::utils::{PowerVector, PowerVectorTrait};
use crate::stat::LinEqResult;

use delegate::delegate;
use getset::{Getters, Setters};

#[derive(Getters, Setters)]
pub struct SampleIsotope {
    #[getset(get = "pub")]
    isotope: PowerVector,
    #[getset(get = "pub")]
    corrected: Option<PowerVector>,
    #[getset(get = "pub")]
    calibrated: Option<PowerVector>,
    #[getset(get = "pub", set = "pub")]
    excess: Option<PowerVector>,
}

impl PowerVectorTrait for SampleIsotope {
    fn with_capacity(size: usize) -> SampleIsotope {
        SampleIsotope {
            isotope: PowerVector::with_capacity(size),
            corrected: None,
            calibrated: None,
            excess: None,
        }
    }
    delegate! {
        to self.isotope {
            fn push(&mut self, value: f64);
            fn iter(&self) -> std::slice::Iter<f64>;
            fn len(&self) -> usize;
        }
    }
}

impl SampleIsotope {
    delegate! {
        to self.isotope {
            pub fn mean(&self) -> &Option<f64>;
            pub fn std(&self) -> &Option<f64>;
            pub fn perform_calcs(&mut self);
        }
    }
    pub fn perform_corrections(&mut self, lin_eq_params: &LinEqResult) {
        if self.corrected.is_none() {
            let mut corrected = PowerVector::with_capacity(self.len());
            for it in self.iter() {
                corrected.push(lin_eq_params.slope * it + lin_eq_params.intercept);
            }
            corrected.perform_calcs();
            self.corrected = Some(corrected);
        }
    }
    pub fn perform_calibration(&mut self, lin_eq_params: &LinEqResult) {
        if self.calibrated.is_none() {
            let mut calibrated = PowerVector::with_capacity(self.len());
            for it in self.iter() {
                calibrated.push(lin_eq_params.slope * *it + lin_eq_params.intercept);
            }
            calibrated.perform_calcs();
            self.calibrated = Some(calibrated);
        }
    }
}
