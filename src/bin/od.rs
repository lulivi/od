use clap;
use od::fixed_values::read_fixed_values;
use od::result::CalculationResults;
use std::fs;

/// Checks if the argument has the `.csv` extension
///
/// # Arguments
///
/// * `file_name` - A string with the file name
fn has_csv_extension(file_name: &str) -> Result<String, String> {
    match file_name.ends_with(".csv") {
        true => Ok(file_name.to_owned()),
        false => Err(format!(
            "El archivo '{}' no tiene extensión 'csv'",
            file_name
        )),
    }
}

/// Returns the argument matches from the command line
pub fn get_matches() -> clap::ArgMatches {
    clap::Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .args(&[
            clap::Arg::new("FIXED_VALUES")
                .help("Indica el archivo de valores fijos")
                .required(true)
                .value_parser(has_csv_extension),
            clap::Arg::new("INPUT")
                .help("Indica el archivo de entrada a usar")
                .required(true)
                .value_parser(has_csv_extension),
            clap::Arg::new("patrones")
                .short('p')
                .help("Indica el número de patrones con las que se quedará el programa (por defecto 7)")
                .action(clap::ArgAction::Set)
                .required(false),
            clap::Arg::new("muestras")
                .short('m')
                .help("Indica el número de muestras con las que se quedará el programa (por defecto 7)")
                .action(clap::ArgAction::Set)
                .required(false),
            clap::Arg::new("limpio").short('l').help("Únicamente sacar csv limpio").action(clap::ArgAction::SetTrue),
        ])
        .get_matches()
}

fn main() {
    let matches = get_matches();
    let input_name: &str = matches.get_one::<String>("INPUT").unwrap();
    let raw_n_patterns: &str = matches.get_one::<&str>("patrones").unwrap_or(&"7");
    let n_patterns: usize = match raw_n_patterns.parse() {
        Ok(value) => value,
        Err(_) => {
            println!(
                "!!> El número de lectura de elementos por cada patrón es inválido: {}",
                raw_n_patterns
            );
            std::process::exit(1);
        }
    };
    let raw_n_samples: &str = matches.get_one::<&str>("muestras").unwrap_or(&"7");
    let n_samples: usize = match raw_n_samples.parse() {
        Ok(value) => value,
        Err(_) => {
            println!(
                "!!> El número de lectura de elementos por cada muestra es inválido: {}",
                raw_n_samples
            );
            std::process::exit(1);
        }
    };
    let fixed_values_path: &str = matches.get_one::<String>("FIXED_VALUES").unwrap();
    let fixed_values_contents = match fs::read_to_string(fixed_values_path) {
        Ok(contents) => contents,
        Err(_) => {
            eprintln!(
                "The fixed values file does not exist `{}`",
                fixed_values_path
            );
            std::process::exit(1);
        }
    };
    let fixed_values: std::collections::HashMap<String, std::collections::HashMap<String, f64>> =
        match read_fixed_values(fixed_values_contents.as_str()) {
            Ok(fixed_values) => fixed_values,
            Err(err) => {
                println!(
                    "!!> Ocurrió un error mientras se leían los valores fijos\n\t{}",
                    err
                );
                std::process::exit(1);
            }
        };

    let mut calculation_results =
        CalculationResults::new(input_name, n_patterns, n_samples, fixed_values);

    if let Err(err) = calculation_results.load_csv() {
        println!("!!> Error leyendo el archivo '{}':\n\t{}", input_name, err);
        std::process::exit(1);
    }

    if *matches.get_one::<bool>("limpio").unwrap() {
        std::process::exit(0);
    }

    if let Err(err) = calculation_results.perform_calcs() {
        println!("!!> Error realizando los cálculos:\n\t{}", err);
        std::process::exit(1);
    }
    if let Err(err) = calculation_results.serialize() {
        println!("!!> Error guardando los resultados:\n\t{}", err);
        std::process::exit(1);
    }
}
