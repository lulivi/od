use crate::result::utils::{PowerVector, PowerVectorTrait};
use crate::stat::LinEqResult;

use delegate::delegate;
use getset::Getters;

#[derive(Getters)]
pub struct PatternIsotope {
    #[getset(get = "pub")]
    isotope: PowerVector,
    #[getset(get = "pub")]
    corrected: Option<PowerVector>,
    #[getset(get = "pub")]
    difference: Option<PowerVector>,
}

impl PowerVectorTrait for PatternIsotope {
    fn with_capacity(size: usize) -> Self {
        PatternIsotope {
            isotope: PowerVector::with_capacity(size),
            corrected: None,
            difference: None,
        }
    }
    delegate! {
        to self.isotope {
            fn push(&mut self, value: f64);
            fn iter(&self) -> std::slice::Iter<f64>;
            fn len(&self) -> usize;
        }
    }
}

impl PatternIsotope {
    delegate! {
        to self.isotope {
            pub fn mean(&self) -> &Option<f64>;
            pub fn std(&self) -> &Option<f64>;
            pub fn perform_calcs(&mut self);
        }
    }
    pub fn perform_corrections(&mut self, lin_eq_params: &LinEqResult) {
        if self.corrected.is_none() || self.difference.is_none() {
            let mut corrected = PowerVector::with_capacity(self.len());
            let mut difference = PowerVector::with_capacity(self.len());
            let isotope_mean = self.isotope.mean().unwrap();
            for pattern_it in self.iter() {
                let corrected_value: f64 =
                    lin_eq_params.slope * pattern_it + lin_eq_params.intercept;
                corrected.push(corrected_value);
                difference.push(corrected_value - isotope_mean)
            }
            corrected.perform_calcs();
            difference.perform_calcs();
            self.corrected = Some(corrected);
            self.difference = Some(difference)
        }
    }
}
