use super::Group;
use crate::result::isotopes::PatternIsotope;
use crate::result::schema::CleanRecord;
use crate::stat::LinEqResult;

use delegate::delegate;

pub struct PatternGroup {
    common: Group<PatternIsotope>,
}

impl PatternGroup {
    pub fn new(queue: &[CleanRecord]) -> Self {
        Self {
            common: Group::<PatternIsotope>::new(queue),
        }
    }
    delegate! {
        to self.common {
            pub fn append(&mut self, queue: &[CleanRecord]);
            pub fn inj_nr(&self) -> &Vec<u8>;
            pub fn d17_16(&self) -> &PatternIsotope;
            pub fn d17_16_mut(&mut self) -> &mut PatternIsotope;
            pub fn d18_16(&self) -> &PatternIsotope;
            pub fn d18_16_mut(&mut self) -> &mut PatternIsotope;
            pub fn dd_h(&self) -> &PatternIsotope;
            pub fn dd_h_mut(&mut self) -> &mut PatternIsotope;
            pub fn identifier_1(&self) -> &String;
        }
    }
    pub fn perform_calcs(&mut self) {
        self.d17_16_mut().perform_calcs();
        self.d18_16_mut().perform_calcs();
        self.dd_h_mut().perform_calcs();
    }
    pub fn perform_corrections(
        &mut self,
        d18_16_lin_eq_result: &LinEqResult,
        dd_h_lin_eq_result: &LinEqResult,
    ) {
        self.d18_16_mut().perform_corrections(d18_16_lin_eq_result);
        self.dd_h_mut().perform_corrections(dd_h_lin_eq_result);
    }
}
