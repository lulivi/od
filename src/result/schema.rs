use serde::Deserialize;
use serde::Serialize;

/// Complete information from the input file about the measure
#[derive(Debug, Deserialize, Serialize)]
pub struct InputRecord {
    #[serde(rename = "Line")]
    pub line: u32,
    #[serde(rename = "Analysis")]
    analysis: String,
    #[serde(rename = "Time Code")]
    time_code: String,
    #[serde(rename = "Port")]
    port: String,
    #[serde(rename = "Inj Nr")]
    pub inj_nr: u8,
    #[serde(rename = "d(17_16)Mean")]
    pub d17_16: f64,
    #[serde(rename = "d(18_16)Mean")]
    pub d18_16: f64,
    #[serde(rename = "d(D_H)Mean")]
    pub dd_h: f64,
    #[serde(rename = "E17_Mean")]
    e17_mean: f64,
    #[serde(rename = "H2O_Mean")]
    h2o_mean: f64,
    #[serde(rename = "Ignore")]
    ignore: i8,
    #[serde(rename = "Good")]
    good: u8,
    #[serde(rename = "Identifier 1")]
    pub identifier_1: String,
    #[serde(rename = "Identifier 2")]
    identifier_2: Option<String>,
    #[serde(rename = "Gas Configuration")]
    gas_configuration: String,
    #[serde(rename = "Timestamp Mean")]
    timestamp_mean: f64,
    #[serde(rename = "d(17_16)_SD")]
    d17_16_sd: f64,
    #[serde(rename = "d(18_16)_SD")]
    d18_16_sd: f64,
    #[serde(rename = "d(D_H)_SD")]
    dd_h_sd: f64,
    #[serde(rename = "E17_SD")]
    e17_sd: f64,
    #[serde(rename = "H2O_SD")]
    h2o_sd: f64,
    #[serde(rename = "d(18_16)_Sl")]
    d18_16_sl: f64,
    #[serde(rename = "d(D_H)_Sl")]
    dd_h_sl: f64,
    #[serde(rename = "H2O_Sl")]
    h2o_sl: f64,
    #[serde(rename = "baseline_shift")]
    baseline_shift: f64,
    #[serde(rename = "slope_shift")]
    slope_shift: f64,
    #[serde(rename = "residuals")]
    residuals: f64,
    #[serde(rename = "baseline_curvature")]
    baseline_curvature: f64,
    #[serde(rename = "interval")]
    interval: f64,
    #[serde(rename = "ch4_ppm")]
    ch4_ppm: f64,
    #[serde(rename = "h16od_adjust")]
    h16od_adjust: f32,
    #[serde(rename = "h16od_shift")]
    h16od_shift: f32,
    #[serde(rename = "n2_flag")]
    n2_flag: u8,
    #[serde(rename = "Resistance")]
    resistance: String,
    #[serde(rename = "DAS Temp")]
    das_temp: f32,
    #[serde(rename = "Tray")]
    tray: u8,
    #[serde(rename = "Sample")]
    sample: u8,
    #[serde(rename = "Job")]
    job: u8,
    #[serde(rename = "Method")]
    method: String,
    #[serde(rename = "Error Code")]
    error_code: u8,
    #[serde(rename = "Pulse Good")]
    pulse_good: u8,
}

/// Clean measure representation
#[derive(Debug, Serialize, Clone)]
pub struct CleanRecord {
    #[serde(rename = "Inj Nr")]
    pub inj_nr: u8,
    #[serde(rename = "d(17_16)Mean")]
    pub d17_16: f64,
    #[serde(rename = "d(18_16)Mean")]
    pub d18_16: f64,
    #[serde(rename = "d(D_H)Mean")]
    pub dd_h: f64,
    #[serde(rename = "Identifier 1")]
    pub identifier_1: String,
}
