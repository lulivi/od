mod pattern;
mod sample;

pub use pattern::PatternGroup;
pub use sample::SampleGroup;

use crate::result::schema::CleanRecord;
use crate::result::utils::PowerVectorTrait;

use delegate::delegate;
use getset::{Getters, MutGetters};

#[derive(Getters, MutGetters)]
struct Group<T> {
    #[getset(get = "pub")]
    inj_nr: Vec<u8>,
    #[getset(get = "pub", get_mut = "pub")]
    d17_16: T,
    #[getset(get = "pub", get_mut = "pub")]
    d18_16: T,
    #[getset(get = "pub", get_mut = "pub")]
    dd_h: T,
    #[getset(get = "pub")]
    identifier_1: String,
}

impl<T> Group<T>
where
    T: PowerVectorTrait,
{
    pub fn new(queue: &[CleanRecord]) -> Self {
        let max_leng = queue.len() * 2;
        let mut new_group = Self {
            inj_nr: Vec::<u8>::with_capacity(max_leng),
            d17_16: T::with_capacity(max_leng),
            d18_16: T::with_capacity(max_leng),
            dd_h: T::with_capacity(max_leng),
            identifier_1: String::with_capacity(0),
        };
        new_group.append(queue);
        new_group
    }
    pub fn append(&mut self, queue: &[CleanRecord]) {
        let mut queue_iter = queue.iter();
        {
            let record = queue_iter.next().unwrap();
            self.identifier_1 = record.identifier_1.clone();
            self.append_record(record);
        }
        for record in queue_iter {
            self.append_record(record);
        }
    }
    fn append_record(&mut self, record: &CleanRecord) {
        self.inj_nr.push(record.inj_nr);
        self.d17_16.push(record.d17_16);
        self.d18_16.push(record.d18_16);
        self.dd_h.push(record.dd_h);
    }
    delegate! {
        to self.inj_nr {
            pub fn len(&self) -> usize;
        }
    }
}
